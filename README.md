# DAM frontend

The goal is to re-write the following repo (this repo)

https://gitlab.com/info_varify/frontend-test

Basically it have to modes right now

1. Normal mode - app.js, this is simply display the code or download link
2. Reveal mode - app-reveal.js this is very similar to the app.js, but added function to able to reveal the code that will open modal need customer to upload file and return the asset

You can try out using the index.html and index-reveal.html

The aim is to rewrite both (app.js and app-reveal.js) and combine them into one react app

- It will need to use vite - [https://vitejs.dev/](https://vitejs.dev/)
- It will need to use vite to generate single file for js and single file for CSS
- It will use typescript
- The app will have a mode setting to show different mode
- Use different git branch for your work
- Make the code more readable
- Share components as much as possible
- If need to use UI framework, please use Ant design

The data should be include in the app.js and app-reveal.js, you can use them as test for now

The Upload file api endpoint, you don’t need to worry about it, just assume the endpoint is working and will return a url
