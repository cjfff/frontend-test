(async function() {
  async function uploadToS3(file, signedRequest) {
    const acl = 'private';
    console.log('_DAM the upload s3', file, signedRequest, acl);
    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': file.type,
        'x-amz-acl': acl,
        ACL: acl,
      },
      body: file,
    };

    // Going to put it to the S3
    const response = await fetch(signedRequest, options);

    // Check if the status is ok
    // S3 return empty body
    return response.status === 200;
  }

  function closeUploadForm(event) {
    if (event && event.preventDefault) {
      event.preventDefault();
    }

    console.log('_DAM going to close this');
    const theFormWrapper = document.getElementById('_dam-upload-form');
    theFormWrapper.innerHTML = '';
  }


  function _damHandleActions(url, data) {
    console.log('_DAM going to handle actions', {
      url,
      data,
    });

    return new Promise(function(resolve, reject) {
      fetch(url, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
      })
        .then(function(res) {
          return res.json();
        })
        .then(function(result) {
          if (result.error) {
            console.log('_DAM there is error', result.error);
            reject(result.error);
          } else {
            console.log('_DAM got the result back from server', result);
            resolve(result);
          }
        })
        .catch(function(error) {
          reject(error);
        })
    })
  }

  // Figure out the path
  const getEndpoint = async () => {
    if (window._dam_endpoint) {
      return window._dam_endpoint;
    }

    const knownPaths = [
      "/a/dam",
      "/apps/digital-assets-manager",
    ];

    let endpoint = false;
    for (let path of knownPaths) {
      const r = await fetch(path, {
        method: 'HEAD',
      });

      console.log('_DAM goign to check path', path);
      if (r.status === 200) {
        endpoint = path;
        break;
      }
    }

    window._dam_endpoint_path = endpoint;

    console.log('_DAM the path', endpoint);
    return endpoint;
  };

  const loadData = async () => {
    return [
      {
        id: "123",
        productName: "t shrt 1",
        orderId: "123",
        orderNumber: "123",
        type: "code",
        code: "1233h1jk2j123",
        //content: '123',
      },
      {
        id: "423",
        productName: "Steering wheel",
        orderId: "423",
        orderNumber: "423",
        type: "file",
        downloadLink: "https://www.example.com",
        //content: "https://www.example.com"
      }
    ];
    const endpoint = await getEndpoint();
    if (window._dam_customer_id && endpoint) {
      try {
        const res = await fetch(endpoint, {
          method: "POST",
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            key: window._dam_customer_id,
            action: 'listing',
          })
        });

        const data = await res.json();

        return data;
      } catch (error) {
        throw new Error('Sorry something wrong while fetching data, Please refresh the page later');
      }
    } else {
      throw new Error('Please login to see your digital assets');
    }
  };

  const uploadFile = async function(file) {
    console.log('_DAM Going to upload file', {
      file,
    });

    const url = window._dam_endpoint_path;
    const res = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        action: 'signS3',
        fileName: file.name,
        fileType: file.type,
      }),
    });

    const result = await res.json();

    if (res.status !== 200) {
      throw new Error(result.error);
    }

    console.log('_DAM the result', result, result.data, result.data.signedRequest);
    if (!result.data.signedRequest) {
      throw new Error('Can not upload file, please try again later');
    }

    await uploadToS3(file, result.data.signedRequest, 'private');

    return {
      url: result.data.url,
      key: result.data.key,
    }
  }

  const renderEntries = (entries) => {
    const entryContent = (data) => {
      console.log('_DAM entry content', data)
      if (data.content) {
        console.log('_DAM data content is true');
      }
      if (data.content !== '') {
        console.log('_DAM the content is not "" string');
      }

      if (data.content) {
        console.log('_DAM the data content', data.content, typeof data.content);
        return data.content;
      } else {
        console.log('_DAM here is the action fefef');
        return `
				<button data-id="${data.id}" class="_dam-reveal-key">Display the asset</button>
				<button data-id="${data.id}" class="_dam-return-key">Return the asset</button>
      `;
      }
    }

    const trs = entries.map((data, index) => `
          <tr className="dam-entry">
              <td>${data.orderNumber}</td>
              <td>${data.productName}</td>
              <td>${data.type}</td>
              <td>${entryContent(data)}</td>
          </tr>
    `);

    return `
      <table>
          <thead>
              <tr>
                  <th>Order</th>
                  <th>Asset name</th>
                  <th>Type</th>
                  <th>Actions/Content</th>
              </tr>
          </thead>
          <tbody>
            ${trs.join('')}
          </tbody>
      </table>
    `
  };

  function makeP(text) {
    const pEl = document.createElement('p');
    pEl.innerText = text;

    return pEl;
  }

  function makeInput(name, type, value, placeholder, includeWrapper = false) {
    const input = document.createElement('input');
    input.type = type;
    input.name = name;

    if (placeholder) {
      input.placeholder = placeholder;
    }

    if (value) {
      input.value = value;
    }

    if (includeWrapper) {
      const wrapper = document.createElement('div');
      wrapper.append(input);
      return wrapper;
    } else {
      return input;
    }
  }

  function renderUploadForm(entryDataId, introText, buttonText, includeFile = true) {
    const uploadWrapper = document.createElement('form');
    uploadWrapper.id = '_dam-upload-form-content';
    uploadWrapper.classList.add('modal-window');

    const content = document.createElement('div');

    const closeButton = document.createElement('a');
    closeButton.href = '#';
    closeButton.innerHTML = 'Close';
    closeButton.classList.add('modal-close');
    closeButton.addEventListener('click', closeUploadForm);
    content.append(closeButton);

    content.append(makeP(introText));

    content.append(makeP('Please confirm your email'));
    content.append(makeInput('email', 'text', '', 'Enter email'));

    if (includeFile) {
      content.append(makeP('Upload an ID e.g Driving license or passport'));
      content.append(makeInput('file', 'file'));
    }

    content.append(makeInput('id', 'hidden', entryDataId));

    content.append(makeInput('submit', 'submit', buttonText, '', includeWrapper = true));

    uploadWrapper.append(content);
    return uploadWrapper;
  }

  const getContent = async () => {
    try {
      // Load the data
      const entries = await loadData();
      console.log('_DAM entries', entries);

      let content = `<p>Sorry no orders with digital asset yet</p>`;
      if (entries.length > 0) {
        content = renderEntries(entries);
      }

      return content;
    } catch (error) {
      console.error('_dam', error);
      return error
    }
  };

  const App = async () => {
    console.log('_DAM going to load data');

    try {
      display('loading');
      const content = await getContent();

      display(`
        ${content}
        <div id="_dam-upload-form"></div>
      `);
    } catch (error) {
      display(`
        <p>Sorry something went wrong, please refresh the page later</p>
        <p>${error}</p>
      `);
    }
  };

  const rootEl = document.getElementById('_dam_root');
  const display = (content) => rootEl.innerHTML = content;
  await App();

  // Reveal and return key actions
  const url = window._dam_endpoint_path;
  const revealBtns = rootEl.querySelectorAll('._dam-reveal-key');
  const uploadForm = document.getElementById('_dam-upload-form');
  for (const btn of revealBtns) {
    btn.addEventListener('click', async () => {
      try {
        const entryDataId = btn.dataset.id;

        // Empty form
        uploadForm.innerHTML = '';

        const introText = 'You almost there, but we need a few more informatioin from you, then we can release the code';
        const formContent = renderUploadForm(entryDataId, introText, 'Get the code');
        formContent.addEventListener('submit', async function(event) {
          event.preventDefault();

          const emailEl = formContent.querySelector('input[name=email]');
          const fileEl = formContent.querySelector('input[name=file]');
          console.log('_DAM going to submit the form', emailEl, fileEl);

          email = emailEl.value;
          if (!email) {
            alert('Sorry please provide an email');
            return;
          }

          const file = fileEl.files[0];
          if (!file) {
            alert('Please upload an file');
            return;
          }

          try {
            // Going to upload the file
            const fileResult = await uploadFile(file);

            console.log('_DAM Going to submit', {
              file,
              email,
              entryDataId,
            });

            const data = await _damHandleActions(url, {
              action: 'claimRequest',
              entryDataId,
              url: fileResult.url,
              key: fileResult.key,
              email,
              fileName: file.name,
              fileType: file.type,
            });

            console.log('close upload form', closeUploadForm);
            closeUploadForm();
            alert('Thanks for that, we will handle this very soon');
          } catch (error) {
            console.error('error while uploading things', error);
            alert(String(error));
          }
        });

        uploadForm.append(formContent);
      } catch (error) {
        alert(String(error));
      }
    });
  }

  const returnBtns = rootEl.querySelectorAll('._dam-return-key');
  for (const btn of returnBtns) {
    btn.addEventListener('click', async () => {
      try {
        const entryDataId = btn.dataset.id;

        // Empty form
        uploadForm.innerHTML = '';

        const introText = 'Returning the asset!';
        const formContent = renderUploadForm(entryDataId, introText, 'Return', false);
        formContent.addEventListener('submit', async function(event) {
          event.preventDefault();

          const emailEl = formContent.querySelector('input[name=email]');
          const fileEl = formContent.querySelector('input[name=file]');
          console.log('going to submit the form', emailEl, fileEl);

          email = emailEl.value;
          if (!email) {
            alert('Sorry please provide an email');
            return;
          }

          try {
            const data = await _damHandleActions(url, {
              action: 'refund',
              entryDataId,
              email,
            });

            closeUploadForm();

            // Reload the page
            alert('Successfully refunded this item');
          } catch (error) {
            console.error('error while returning the asset', error);
            alert(String(error));
          }
        });

        uploadForm.append(formContent);
      } catch (error) {
        alert(String(error));
      }
    });
  }
})();
